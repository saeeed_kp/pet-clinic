package org.springframework.samples.petclinic.utility;

import org.springframework.samples.petclinic.owner.PetRepository;

import javax.validation.constraints.Null;
import java.util.HashMap;
import java.util.concurrent.Callable;

/**
 * this simple class shows the main idea behind a Dependency Injection library
 */
public class SimpleDI {

	private static SimpleDI singleton;
	private static HashMap<Class<?>, Object> instanceMap;

	public static SimpleDI getDIContainer() {
		if(singleton == null){
			singleton = new SimpleDI();
			instanceMap = new HashMap<>();
		}
		return singleton;
	}

	public void provideByInstance(Class<?> typeClass, Object instanceOfType){
		if(!instanceMap.containsKey(typeClass)){
			instanceMap.put(typeClass, instanceOfType);
		}
	}

	public void provideByAConstructorFunction(Class<?> typeClass, Callable<Object> providerFunction) throws Exception {
		if(!instanceMap.containsKey(typeClass)){
			instanceMap.put(typeClass, providerFunction.call());
		}
	}

	public Object getInstanceOf(Class<?> requiredType) throws Exception{
		if(!instanceMap.containsKey(requiredType)){
			throw new Exception();
		}
		return instanceMap.get(requiredType);
	}
}
