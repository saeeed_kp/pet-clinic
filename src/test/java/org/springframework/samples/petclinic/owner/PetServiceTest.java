package org.springframework.samples.petclinic.owner;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.samples.petclinic.owner.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.samples.petclinic.utility.PetTimedCache;
import org.springframework.samples.petclinic.utility.SimpleDI;

import org.slf4j.Logger;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
class PetServiceTest {

	@Mock
	private PetTimedCache pets;
	@Mock
	private  OwnerRepository owners;
	@Mock
	private Logger log;
	@InjectMocks
	private PetService petservice;

	@Before
	public void setUp() {
		pets = Mockito.spy(PetTimedCache.class); //spy object
		owners = Mockito.spy(OwnerRepository.class); //spy object
		log = Mockito.spy(Logger.class); //spy object
		petservice = new PetService(pets, owners, log); //fake object
	}

	@After
	public void tearDown() {
		pets = null;
		owners = null;
		log = null;
		petservice = null;
	}

	//behavior verification
	//Mockisty
	@Test
	public void findOwnerCheckIfFindProperOwnerAndCallSuccessfullyTest(){
		Owner expectedOwner = new Owner(); //fake object
		expectedOwner.setId(1);
		Mockito.when(owners.findById(1)).thenReturn(expectedOwner);
		Owner owner = petservice.findOwner(1);
		assertEquals(expectedOwner, owner);
		Mockito.verify(log).info("find owner {}", 1);
	}

	//behavior verification
	//Mockisty
	@Test
	public void newPetCheckIfAddPetCallSuccessfullyTest() {
		Owner owner = Mockito.mock(Owner.class); //mock object
		petservice.newPet(owner);
		Mockito.verify(log).info("add pet for owner {}", 1);
	}

	//behavior verification
	//Mockisty
	@Test
	public void findPetCheckIfFindProperPetAndCallSuccessfullyTest() {
		Pet expectedPet = new Pet(); //fake object
		expectedPet.setId(1);
		Mockito.when(pets.get(1)).thenReturn(expectedPet);
		Pet pet = petservice.findPet(1);
		assertEquals(expectedPet, pet);
		Mockito.verify(log).info("find pet by id {}", 1);
	}

	//behavior verification
	//Mockisty
	@Test
	public void savePetCheckIfSaveCorrectlyAndCallSuccessfullyTest() {
		Pet pet = Mockito.mock(Pet.class); //mock object
		Owner owner = Mockito.mock(Owner.class);
		Mockito.when(pet.getId()).thenReturn(1);
		petservice.savePet(pet, owner);
		Mockito.verify(log).info("save pet {}", 1);
	}
}

